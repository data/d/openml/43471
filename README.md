# OpenML dataset: Candy-crush

https://www.openml.org/d/43471

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Candy Crush Saga is a hit mobile game developed by King (part of ActivisionBlizzard) that is played by millions of people all around the world. The game is structured as a series of levels where players need to match similar candy together to (hopefully) clear the level and keep progressing on the level map. If you are one of the few that haven't played Candy Crush

Content
Candy Crush has more than 3000 levels, and new ones are added every week. That is a lot of levels! And with that many levels, it's important to get level difficulty just right. Too easy and the game gets boring, too hard and players become frustrated and quit playing.

Acknowledgements
DataCamp

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43471) of an [OpenML dataset](https://www.openml.org/d/43471). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43471/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43471/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43471/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

